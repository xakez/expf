
var startX = -1;

$(document).ready(function() {
   resizer();
   
   
   $(window).resize(function(){
      resizer();
   });


   $('.prev-sl').click(function(){
      var sl = $(this).closest(".news-sl"),
         newsSl = sl.find('.news-slides').eq(0),
         n = parseInt(newsSl.attr("num"),10),
         currM = parseInt(newsSl.css("margin-top"),10);
      if (!n){
         n=0;
      }
      n--;

      if (sl.length===0){
         sl = $(this).closest(".tweet-slide");
         newsSl = sl.find('.news-slides').eq(0);
         currM = parseInt(newsSl.css("margin-top"),10);
         currM=-172*n;
      } else
         currM=-147*n;


      if (currM>0) {
         currM = 0;
         n = 0;
      }
      newsSl.attr("num", n);
      newsSl.stop().animate({"margin-top":currM+"px"});

   });

   $('.next-sl').click(function(){
      var sl = $(this).closest(".news-sl"),
         newsSl = sl.find('.news-slides').eq(0),
         n = parseInt(newsSl.attr("num"),10),
         h = newsSl.height(),
         currM = parseInt(newsSl.css("margin-top"),10);

      if (!n){
         n=0;
      }

      n++;

      if (sl.length===0){
         sl = $(this).closest(".tweet-slide");
         newsSl = sl.find('.news-slides').eq(0);
         currM = parseInt(newsSl.css("margin-top"),10);
         currM = -172*n;
         if (currM-172<h*(-1)){
            currM = -h + 172;
            n--;
         }
      } else {

         currM = -147*n;

         if (currM-147<h*(-1)){
            currM = -h + 147;
            n--;
         }
      }
      newsSl.stop().animate({"margin-top":currM+"px"});
      newsSl.attr("num", n);
   });

   $('.style-input').focus(function(){
         var def = $(this).attr("defvalue"),
            val = $(this).val();
         if (def===val){
            $(this).val('');

         }
         $(this).addClass('with-val');
      });

   $('.style-input').blur(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });

   $('.one-switch-sl').click(function(){
      var sls = $(this).closest('.slider');
      sls.find('.one-switch-sl').removeClass("active");
      $(this).addClass("active");
      sls.css("background", "url('"+$(this).attr('big')+"') no-repeat");
   });


   $('.one-tab').click(function(){
      var id = $(this).attr('numid');
      $('.one-tab').removeClass("active");
      $(this).addClass("active");
      $('.slider').stop().animate({opacity:0});
      $('.slider').removeClass("active");
      $('.one-tab-content').stop().animate({opacity:0}, function(){
         $('.one-tab-content:not(#tabcont'+id+')').removeClass("active");
      });

      for (var o=0;o<$('.one-tab-content .text-tab-content').length;o++) {
         $('#' + $('.one-tab-content .text-tab-content').eq(o).getNiceScroll()[0].id).hide();
      }
      $('.slider#slider'+id).stop().animate({opacity:1});
      $('.slider#slider'+id).addClass("active");
      $('.one-tab-content#tabcont'+id).addClass("active");
      $('.one-tab-content#tabcont'+id).stop().animate({opacity:1});
      $('#'+$('.one-tab-content#tabcont'+id+' .text-tab-content').getNiceScroll()[0].id).show();

      //$('.one-tab-content#tabcont'+id+' .text-tab-content').getNiceScroll()[0].show();
   });

   $('.slider-usl .prev-sl').click(function(){
      var sl = $(this).closest(".slider-usl"),
         newsSl = sl.find('.all-service-in-slider').eq(0),
         n = parseInt(newsSl.attr("num"),10),
         h = newsSl.height(),
         currM = parseInt(newsSl.css("margin-top"),10);
      if (!n){
         n=0;
      }
      n--;
      currM=-120*n;


      if (currM>0) {
         currM = 0;
         n = 0;
      }
      newsSl.attr("num", n);
      newsSl.stop().animate({"margin-top":currM+"px"});
   });

   $('.slider-usl .next-sl').click(function(){
      var sl = $(this).closest(".slider-usl"),
         newsSl = sl.find('.all-service-in-slider').eq(0),
         n = parseInt(newsSl.attr("num"),10),
         h = newsSl.height(),
         currM = parseInt(newsSl.css("margin-top"),10);

      if (!n){
         n=0;
      }
      n++;

      currM = -120*n;

      if (currM-120<h*(-1)){
         currM = -h + 120;
         n--;
      }
      newsSl.attr("num", n);
      newsSl.stop().animate({"margin-top":currM+"px"});
   });

   $('.one-tab2').click(function(){
      var tabs = $(this).closest('.tabs2');

      tabs.find('.one-tab2').removeClass("active");

      $(this).addClass("active");


      tabs.find('.tabs2-content').removeClass("active");
      tabs.find('.tabs2-content#'+$(this).attr("tabid")).addClass("active");
   });

   $('.title-list').click(function(){
      var ol = $(this).closest('.one-list');
      if (ol.closest('.open-list').hasClass("right-style")){
         location.href = $(this).find("a").attr("href");
      } else
         ol.toggleClass("active");
   });

   $('.submit-but').click(function(){
      $(this).closest("form").submit();
   });


   $('.combobox').click(function(ev){
      $('.combobox').removeClass('clicked');
      $(this).addClass('clicked');
      ev.stopImmediatePropagation();
   });

   $('body').click(function(){
      $('.combobox').removeClass('clicked');
   });

   $('.one-val').click(function(ev){
      ev.stopImmediatePropagation();
      var cb = $(this).closest(".combobox");
      cb.removeClass("clicked");
      cb.find("span").text($(this).text());
      cb.find("input").val($(this).attr("val"));
   });

   $('.filechoose').click(function(){
      $(this).next('.select-file').trigger("click");
   });

   var activeLoad = 0;
   $('.light-tooltip').hover(function(){
      var img = $(this).attr("img"),
         self = $(this),
         im = new Image();

      activeLoad = 1;

      im.onload = function(){
         setTimeout(function(){
            if (activeLoad){
               var offset = self.offset(),
                  left = offset.left,
                  height = im.height,
                  top1 = offset.top - 10,
                  top = offset.top - height - 10;

               var d  = $("<div class='tooltip'></div>");
               d.append(im);


               //d.css({left: left+"px", top: top1+"px", height: 0});
               d.css({left: left+"px", top: top+"px", height: height+"px"});
               $('body').append(d);
               setTimeout(function(){
                  d.addClass("active");
               }, 50);

               //d.animate({top: top+"px", height:height+"px"});
            }
         },50);

      };

      im.src=img;
   }, function(){
      activeLoad = 0;
      $('.tooltip').remove();
   });

   if ($('.rules').length>0){
      $('.rules').niceScroll({cursorcolor : "#3965af",
         cursorwidth : 23,
         cursorfixedheight : 23,
         cursorborder : "none",
         cursorborderradius : "12px",
         cursorminheight: 23,
         cursoropacitymin: 0.5,
         background: "url('img/scroll.png')"
      });
   }


   if ($('.text-tab-content').length>0){
      for (var k=0;k<$('.text-tab-content').length;k++) {
         $('.text-tab-content').eq(k).niceScroll({
            cursorcolor: "#babdc2",
            cursorwidth: 23,
            cursorfixedheight: 23,
            cursorborder: "none",
            cursorborderradius: "12px",
            cursorminheight: 23,
            cursoropacitymin: 0.5,
            background: "url('img/scroll.png')"
         });
      }
   }



   $('.one-head-gray-tab').click(function(){
      $('.one-head-gray-tab').removeClass("active");
      $('.one-gray-tab').removeClass("active");

      $(this).addClass("active");
      $('.one-gray-tab#'+$(this).attr('tabid')).addClass("active");
   });




   $('.one-preview').click(function(){
      activatePreview($(this));
   });

   $('.one-preview').hover(function(){
      scrollGal($(this));

   });

   $('.left-arr').click(function(){
      var gal = $(this).closest('.photogallery'),
         preview = gal.find('.one-preview.active');

      preview.prev('.one-preview').trigger("click");
   });

   $('.right-arr').click(function(){
      var gal = $(this).closest('.photogallery'),
         preview = gal.find('.one-preview.active');

      preview.next('.one-preview').trigger("click");
   });

   $('.close-gal').click(function(){
      var gal = $(this).closest('.photogallery');
      gal.hide();
      $('body').removeClass("nooverflow");
   });

   $('.opengallery').click(function(){
      openGal($(this).attr('idgallery'));
   });

   $('.next-m').click(function(){
      if (isgo)
         return;

      isgo = 1;

      getM(1, function(html, mt){
         $('.one-title-m').html(mt);
         $('.days-cal').css("overflow","hidden");
         $('.days-cal').append(html);
         $('.days-m').eq(0).animate({opacity:0});
         $('.days-m').eq(1).animate({opacity:1}, function(){
            $('.days-m').eq(0).remove();
            isgo = 0;
            $('.days-cal').animate({height:$('.days-m').height()+"px"});
            $('.days-cal').css("overflow","visible");
         });
      })

   });


   $('.but-dir').click(function(ev){
      ev.stopImmediatePropagation();
      $(this).closest('.one-search').find('.one-search-dir-active').show();
   });
   
   $('.close-dir').click(function(ev){
      ev.stopImmediatePropagation();
      $(this).closest('.one-search').find('.one-search-dir-active').hide();
   });

   $('.prev-m').click(function(){
      if (isgo)
         return;

      isgo = 1;

      getM(-1, function(html, mt){
         $('.one-title-m').html(mt);
         $('.days-cal').append(html);
         $('.days-m').eq(0).animate({opacity:0});
         $('.days-m').eq(1).animate({opacity:1}, function(){
            $('.days-m').eq(0).remove();
            isgo = 0;
            $('.days-cal').animate({height:$('.days-m').height()+"px"});
         });
      })

   });

   $('.more-news .button a').click(function(){
      $('.more-news .button').trigger("click");
      return false;
   });

   $('.more-news .button').click(function(){
      getNews(function(html){
         $('.for-all-news').append($("<div>"+html+"</div>"));
      });
   });


   prepareCal();
});

var bloked = 0;
var interb ;
function prepareCal(){
   $('.one-day.active').hover(function(){
         var inf = $(this).find('.one-full-info-day'),
            offset = $(this).offset(),
            t= offset.top,
            l = offset.left;

         inf.css({
            top: -176+"px",
            left: -405+"px"
         });
         inf.show();


         inf.find('.info-scroll').niceScroll({
            cursorcolor: "#3965af",
            cursorwidth: 23,
            cursorfixedheight: 23,
            cursorborder: "none",
            cursorborderradius: "12px",
            cursorminheight: 23,
            cursoropacitymin: 0.5,
            background: "url('img/scroll.png')"
         });
         
         var a1 = $(this).find('.info-scroll').getNiceScroll()[0].id,
            el1 = $('#'+a1),
            el2 = $('#'+a1+"-hr");
         $(this).find('.one-full-info-day').append(el1);
         $(this).find('.one-full-info-day').append(el2);
            
          interb =  setInterval(function(){
         el1.css({top: 64, left: 450});
         el2.css({top: 64, left: 450});
         }, 100);
      },
      function(){
            
         if (!bloked){
            clearInterval(interb);
            $(this).find('.one-full-info-day').hide();
            $(this).find('.info-scroll').getNiceScroll()[0].remove();
         }
      });



}

var isgo = 0;

function getNews(func){
   setTimeout(function(){
      var html = "";
      var o = $('.one-news-list').eq(0)[0].outerHTML;
      for (var i=0;i<5;i++){
         html+=o;
      }
      func(html);
   }, 1000);
}

function openGal(id){
   $('body').addClass("nooverflow");

   initPhoto(id);

   $('#'+id).show();
}

function scrollGal(pr){
   var left = pr.offset().left,
      phG = pr.closest('.photogallery'),
      ml = parseInt(phG.find('.previews').css("margin-left"),10),
      wid = phG.find('.previews').width(),
      maxML = $('body').width()-wid+148,
      to = ($('body').width()-148) / 2;


   var newML = ml-left+to;
   if (newML>0){
      newML = 0;
   }

   if (newML<maxML){
      newML=maxML;
   }

   phG.find('.previews').stop().animate({"margin-left":newML+"px"}, 5000);
}

function initPhoto(idGal){
   var gal = $('#'+idGal),
      preview = gal.find('.one-preview'),
      pr = preview.eq(0);

   gal.find('.previews').css({width:preview.length*148+"px", "margin-left":0});

   activatePreview(pr);
}

var hashActive = "";

function createHash(){
   hashActive = "";
   for (var i=0;i<5;i++){
      hashActive += String(Math.random());
   }
}


function getM(napr, func){
   setTimeout(function(){

      var pre = parseInt(Math.random()*7);
      var all = 0;
      var lll = "";
      for (var i=0;i<pre;i++){
         all++;
         var oneDay  = '<div class="one-day dis"><span>'+(31-pre+i)+'</span></div>';
         lll+=oneDay;
      }

      for (var j=1;j<32;j++){
         all++;
         var oneDay  = '<div class="one-day"><span>'+(j)+'</span></div>';
         lll+=oneDay;
      }

      if (all<=35) {
         for (var k = all, t = 1; k < 35; k++, t++) {
            var oneDay = '<div class="one-day dis"><span>' + (t) + '</span></div>';
            lll += oneDay;
         }
      } else {
         for (var k = all, t = 1; k < 42; k++, t++) {
            var oneDay = '<div class="one-day dis"><span>' + (t) + '</span></div>';
            lll += oneDay;
         }
      }


      func("<div class='days-m'>"+lll+"<div class='clear'></div></div>", "Март 2014");

   }, 100);
}

function activatePreview(pr){

   createHash();

   var phG = pr.closest('.photogallery'),
      imgSrc = pr.find('img').attr("big"),
      zamk = hashActive,
      img = new Image();

   phG.find('.one-preview').removeClass("active");
   pr.addClass("active");
   var bp = phG.find('.big-photo');
   img.onload = function(){
      if (zamk===hashActive){


         var h = img.height,
            w = img.width,
            wid = $('body').width()-240,
            hei = $('body').height()-148,
            perb = wid/hei,
            per = w/h,
            newW = wid,
            newH = newW / per;


         if (newW>w){
            newW = w;
            newH = newW / per;
         }

         if (newH>hei){
            newH = hei;
            newW = newH / per;

            if (newH>h){
               newH = h;
               newW = newH / per;
            }
         }




         bp.append(img);
         bp.animate({width:newW+"px", height:newH+"px", left: ((wid-newW)/2+120)+"px", top: ((hei-newH)/2)+"px"});
         bp.find('img').eq(0).stop().animate({opacity:0});
         $(img).stop().animate({opacity:1}, function(){
            var iii = bp.find('img');
            if (iii.length>1)
               iii.eq(0).remove();
            calcArr(pr);
            scrollGal(pr);
         });


      }
   };

   img.src = imgSrc;
}


function calcArr(pr){
   var phG = pr.closest('.photogallery'),
      bp = phG.find('.big-photo'),
      w = bp.offset().left;

   if (w<50) w=50;

   phG.find(".left-arr").stop().animate({width:w+"px"});
   phG.find(".right-arr").stop().animate({width:w+"px"});
}

function validate(form_id,email) {
   
   var address = document.forms[form_id].elements[email].value;
   if(reg.test(address) == false) {
      alert('Введите корректный e-mail');
      return false;
   }
}


function resizer(){
   setTimeout(function(){
      var w = $('body').width(),
         h = $(document).height();


   }, 1);
}



//document.onmousemove = mouseMove;

function mouseMove(event){
   event = fixEvent(event);
   var newX = event.pageX - $('.subscribe').offset().left;//-document.body.scrollTop;
   if (startX === -1)
      startX === newX;

   var mar = parseInt((newX -startX)/30,10);

   if (mar){

      $('.subscribe').css("background-position", (mar)+"px 0");

   }
}

function fixEvent(e) {
   // получить объект событие для IE
   e = e || window.event;

   // добавить pageX/pageY для IE
   if ( e.pageX == null && e.clientX != null ) {
      var html = document.documentElement;
      var body = document.body;
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
   }

   // добавить which для IE
   if (!e.which && e.button) {
      e.which = (e.button & 1) ? 1 : ( (e.button & 2) ? 3 : ( (e.button & 4) ? 2 : 0 ) );
   }

   return e;
};



